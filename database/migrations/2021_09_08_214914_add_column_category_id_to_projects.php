<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCategoryIdToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            //

//            1
//            $table->unsignedBigInteger('category_id')->nullable()->after('id');
//
//            $table->foreign('category_id')->references('id')->on('categories');
//            2
//            $table->foreignId('category_id')->constrained();
//            3
//            $table->foreignId('category_id')->constrained('categories');
//            4;
            $table->foreignId('category_id')->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('set null');
//            5
//            $table->foreignId('category_id')
//                ->nullable()
//                ->constrained();
//            $table->foreignId('category_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            //
            $table->dropForeign(['category_id']);
        });
    }
}
