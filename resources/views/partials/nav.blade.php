<nav class="navbar navbar-light navbar-expand-lg bg-white shadow-sm">
    <div class="container">
        <button class="navbar-toggler" type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                data-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="{{__('Toggle navigation')}}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link {{request()->routeIs('home')?'active':''}}" href="{{ route('home') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('contacts.*')?'active':''}}" href="{{ route('contacts.index') }}">Contact</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('departments.*')?'active':''}} " href="{{ route('departments.index') }}">Departamentos</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('about')?'active':''}} " href="{{ route('about') }}">About</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('projects.*')?'active':''}} " href="{{ route('projects.index') }}">Projects</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('categories.*')?'active':''}} " href="{{ route('categories.index') }}">Categorias</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('notifications.*')?'active':''}} " href="{{ route('notifications.index') }}">Notificaciones</a></li>
                <li class="nav-item"><a class="nav-link {{request()->routeIs('tasks.*')?'active':''}} " href="{{ route('tasks.index') }}">Tareas</a></li>
            </ul>
        </div>
    </div>

</nav>

