@extends('layout')
@section('title','Categorias')

@section('content')

    <div class="container">

        <h1>Categorias</h1>
        <a class="btn btn-primary" href="{{route('categories.create')}}">Crear Categoria</a>

        @if(count($categories)>0)
            <div class="mt-3">
                <table class="table table-dark table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th class="w-25">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td class="mr-2">{{$category->name}}</td>

                            <td class="w-25">
                                <div>
                                    <a class="btn btn-light" href="{{route('categories.show', $category)}}">Ver</a>
                                    <a class="btn btn-primary" href="{{route('categories.edit', $category)}}">Editar</a>
                                    <form action="{{route('categories.destroy', $category)}}" method="POST" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger d-flex">Eliminar</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <span>No existen categorias</span>
                    @endforelse

                    </tbody>
                </table>
            </div>
        @endif

    </div>


@endsection
