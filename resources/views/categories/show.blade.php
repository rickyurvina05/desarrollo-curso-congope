@extends('layout')
@section('title',$category->name)

@section('content')
    {{--    @include('partials.session-status')--}}
    <div class="container">
        <div class="card p-5">
            <a href="{{route('categories.index')}}">Categorias</a>
            <h2 class="display-4">Nombre de la categoria : <small>{{$category->name}}</small></h2>
            <p class="text-black-50">Actualizado hace: {{$category->updated_at->diffForHumans()}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <a class="btn btn-info" href="{{route('categories.edit', $category)}}">Editar</a>
                <form class="" action="{{route('categories.destroy', $category)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Eliminar</button>
                </form>
            </div>
            <hr>
            <div class="row">
                @foreach($projects as $project)
                    <div class="col-3">

                        <div class="card">
                            <h2 class="text-secondary">
                                <a href="{{route('projects.show', $project)}}"> {{$project->title}}</a>
                            </h2>
                            <p class="text-secondary">{{$project->description}}</p>
                        </div>

                    </div>
                @endforeach
            </div>

        </div>
    </div>

@endsection
