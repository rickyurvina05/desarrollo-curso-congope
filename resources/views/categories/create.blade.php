@extends('layout')
@section('title','Crear Categoria')

@section('content')

    <div class="container">
        <a href="{{route('categories.index')}}">Categorias</a>

        <div class="row">
            <div class="col-12 col-sm-3 col-lg-6 mx-auto">

                <form class="bg-white shadow rounded py-3" action="{{route('categories.store')}}" method="POST">
                    <h3 class="display-4">Crear Categoria</h3>

                    @csrf
                    <div class="form-group ml-4 ">
                        <label for="">Nombre de la categoria
                            <input class="form-control bg-light shadow-sm  @error('name') is-invalid @else border-0   @enderror" type="text" name="name" value="{{old('name')}}"
                                   placeholder="Nombre...">
                            @error('name')
                            <span class="span invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                             </span>
                            @enderror
                        </label>
                    </div>

                    <button class="btn btn-success btn-md btn-block text-center">Guardar</button>
                </form>
            </div>
        </div>

    </div>

@endsection
