@extends('layout')
@section('title','Editar categoria')

@section('content')
    {{--    @include('partials.session-status')--}}
    <a href="{{route('categories.index')}}">Categorias</a>

    <h1>Editar Categoria: {{$category->name}}</h1>
    <br>
    <form action="{{route('categories.update', $category)}}" method="POST">
        @method('PUT')
        @csrf
        <label for="">Titulo de la categoria
            <br>
            <input type="text" name="name" value="{{old('name', $category->name)}}">
            <br>
            {!!  $errors->first('name','<small>:message</small><br>') !!}
        </label>
        <br>
        <button>Actualizar</button>
    </form>
@endsection
