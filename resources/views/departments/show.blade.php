@extends('layout')
@section('title',$department->title)

@section('content')
    {{--    @include('partials.session-status')--}}
    <div class="container">
        <div class="card p-5">
            <a href="{{route('departments.index')}}">Departamentos</a>

            <h2 class="display-4"><small>{{$department->name}}</small></h2>
            <p class="text-secondary">{{$department->description}}</p>
            <p class="text-black-50">Actualizado hace: {{$department->updated_at->diffForHumans()}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <a class="btn btn-info" href="{{route('departments.edit', $department)}}">Editar</a>
                <form class="" action="{{route('departments.destroy', $department)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Eliminar</button>
                </form>
            </div>
            <hr>
            <h1 class="text-center bg-light">Usuarios del departamento</h1>
            <div class="row">
                @foreach($contacts as $contact)
                    <div class="col-3">

                        <div class="card">
                            <h2 class="text-secondary">
                                <a href="{{route('contacts.show', $contact)}}"> {{$contact->name}}</a>
                            </h2>
                            <small class="text-secondary">{{$contact->email}}</small>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>


    </div>

@endsection
