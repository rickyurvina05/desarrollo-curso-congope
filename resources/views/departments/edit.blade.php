@extends('layout')
@section('title','Editar departamento')

@section('content')
{{--    @include('partials.session-status')--}}
<a href="{{route('departments.index')}}">Departamentos</a>

    <h1>Editar  Proyecto: {{$department->title}}</h1>
    <br>

    <form action="{{route('departments.update', $department)}}" method="POST">
        @method('PUT')
        @csrf
        <label for="">Nombre del departamento
            <br>
            <input type="text" name="name" value="{{old('name', $department->name)}}">
            <br>
            {!!  $errors->first('name','<small>:message</small><br>') !!}
        </label>
        <br>
        <label for="">Descripccion del departamento
            <br>
            <textarea name="description">{{old('description')!=null ? old('description'):$department->description }}</textarea>
            <br>
            {!!  $errors->first('description','<small>:message</small><br>') !!}
        </label>
        <br>
        <button>Actualizar</button>
    </form>
@endsection
