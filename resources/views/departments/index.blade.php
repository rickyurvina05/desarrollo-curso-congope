@extends('layout')
@section('title','Departamentos')

@section('content')

    <div class="container">

        <h1>Departamentos</h1>
        <a class="btn btn-primary" href="{{route('departments.create')}}">Crear Departamento</a>

        @if(count($departments)>0)
            <div class="mt-3">
                <table class="table table-dark table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripccion</th>
                        <th class="w-25">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($departments as $department)
                        <tr>
                            <td class="mr-2">{{$department->name}}</td>

                            <td>{{$department->description}}</td>
                            <td class="w-25">
                                <div>
                                    <a class="btn btn-light" href="{{route('departments.show', $department)}}">Ver</a>
                                    <a class="btn btn-primary" href="{{route('departments.edit', $department)}}">Editar</a>
                                    <form action="{{route('departments.destroy', $department)}}" method="POST" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger d-flex">Eliminar</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <span>No existen departamentos</span>
                    @endforelse

                    </tbody>
                </table>
            </div>
        @endif

    </div>


@endsection
