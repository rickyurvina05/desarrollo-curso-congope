@extends('layout')
@section('title','Portfolio')

@section('content')

    <div class="container">

        <h1>Proyectos</h1>
        <a class="btn btn-primary" href="{{route('projects.create')}}">Crear Proyecto</a>

        {{--        <select name="" id="">--}}
        {{--            @foreach($categories as $index => $category)--}}
        {{--                <option value="{{$index}}">{{$category}}</option>--}}
        {{--            @endforeach--}}
        {{--        </select>--}}


        @if(count($projects)>0)
            <div class="mt-3">
                <table class="table table-dark table-striped">
                    <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Categoria</th>
                        <th>Descripccion</th>
                        <th>Costo</th>
                        <th class="w-25">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($projects as $project)
                        <tr>
                            <td class="mr-2">{{$project->title}}</td>
                            <td>
                                @if($project->category_id)
                                    <a href="{{route('categories.show',$project->category )}}">{{$project->category->name}}</a>
                                @endif
                            </td>
                            <td>{{$project->description}}</td>
                            <td>{{$project->cost}}</td>
                            <td class="w-25">
                                <div>
                                    <a class="btn btn-light" href="{{route('projects.show', $project)}}">Ver</a>
                                    <a class="btn btn-primary" href="{{route('projects.edit', $project)}}">Editar</a>
                                    <form action="{{route('projects.destroy', $project)}}" method="POST" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger d-flex">Eliminar</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <span>No existen proyectos</span>
                    @endforelse

                    </tbody>
                </table>
            </div>
        @endif

    </div>


@endsection
