@extends('layout')
@section('title','Portfolio')

@section('content')
{{--    @include('partials.session-status')--}}
<a href="{{route('projects.index')}}">Proyectos</a>

    <h1>Editar  Proyecto: {{$project->title}}</h1>
    <br>

    <form action="{{route('projects.update', $project)}}" method="POST">
        @method('PUT')
        @csrf
        <label for="">Titulo del proyecto
            <br>
            <input type="text" name="title" value="{{old('title', $project->title)}}">
            <br>
            {!!  $errors->first('title','<small>:message</small><br>') !!}
        </label>
        <br>
        <label for="">Descripccion del proyecto
            <br>
            <textarea name="description">{{old('description')!=null ? old('description'):$project->description }}</textarea>
            <br>
            {!!  $errors->first('description','<small>:message</small><br>') !!}
        </label>
        <br>
        <div class="form-group ml-4 ">
            <label for="category_id">Categoria del proyecto
                <select class="form-control bg-light shadow-sm  @error('category_id') is-invalid @else border-0  @enderror" name="category_id" id="category_id">
                    <option value="">--Selecione--</option>

                    @foreach($categories as $index => $category)
                        <option value="{{$index}}" @if($project->category_id==$index) selected   @endif>{{$category}}</option>
                    @endforeach
                    {{--                                <option value="1">Categoria 1</option>--}}
                    {{--                                <option value="2">Categoria 2</option>--}}
                    {{--                                <option value="3">Categoria 3</option>--}}
                </select>

                @error('category_id')
                <span class="span invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </label>
        </div>
        <button>Actualizar</button>
    </form>
@endsection
