@extends('layout')
@section('title','Portfolio')

@section('content')

    <div class="container">
        <a href="{{route('projects.index')}}">Proyectos</a>

        <div class="row">
            <div class="col-12 col-sm-3 col-lg-6 mx-auto">

                <form class="bg-white shadow rounded py-3" action="{{route('projects.store')}}" method="POST">
                    <h3 class="display-4">Crear Proyecto</h3>

                    @csrf
                    <div class="form-group ml-4 ">
                        <label for="">Titulo del proyecto
                            <input class="form-control bg-light shadow-sm  @error('title') is-invalid @else border-0   @enderror" type="text" name="title" value="{{old('title')}}"
                                   placeholder="Nombre...">
                            @error('title')
                            <span class="span invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </label>
                    </div>

                    <div class="form-group ml-4">
                        <label for="">Descripccion del proyecto
                            <textarea class="form-control bg-light shadow-sm  @error('description') is-invalid @else border-0  @enderror" placeholder="Descipccion del proyecto.."
                                      name="description">{{old('description')}}</textarea>
                            @error('description')
                            <span class="span invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </label>
                    </div>

                    <div class="form-group ml-4 ">
                        <label for="category_id">Categoria del proyecto
                            <select class="form-control bg-light shadow-sm  @error('category_id') is-invalid @else border-0  @enderror" name="category_id" id="category_id">
                                <option value="">--Selecione--</option>

                                @foreach($categories as $index => $category)
                                    <option value="{{$index}}" {{old('category_id')==$index?'selected':''}}>{{$category}}</option>
                                @endforeach
                                {{--                                <option value="1">Categoria 1</option>--}}
                                {{--                                <option value="2">Categoria 2</option>--}}
                                {{--                                <option value="3">Categoria 3</option>--}}
                            </select>

                            @error('category_id')
                            <span class="span invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                    </div>
                    <button class="btn btn-success btn-md btn-block text-center">Guardar</button>
                </form>
            </div>
        </div>

    </div>

@endsection
