@extends('layout')
@section('title',$project->title)

@section('content')
    {{--    @include('partials.session-status')--}}
    <div class="container">
        <div class="card p-5">
            <a href="{{route('projects.index')}}">Proyectos</a>

            <h2 class="display-4">Titulo del proyecto : <small>{{$project->title}}</small></h2>
            @if($project->category_id)
            <small>
                <a href="{{route('categories.show',$project->category )}}">{{$project->category->name ?? ''}}</a>
            </small>
            @else
                <strong>Sin Categoria</strong>
            @endif
            <p class="text-secondary">{{$project->description}}</p>
            <p class="text-black-50">Actualizado hace: {{$project->updated_at->diffForHumans()}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <a class="btn btn-info" href="{{route('projects.edit', $project)}}">Editar</a>
                <form class="" action="{{route('projects.destroy', $project)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Eliminar</button>
                </form>
            </div>
        </div>
    </div>

@endsection
