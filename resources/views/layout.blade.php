<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>@yield('title', 'CONGOPE')</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--    //BOOTSTRAP--}}
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

{{--    <link rel="stylesheet" href="/css/app.css">--}}

{{--    <script src="/js/app.js" defer></script>--}}

<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="d-flex flex-column justify-content-between " style="height: 100vh;">
    <header>
        @include('partials.nav')
        @include('partials.session-status')

    </header>
    <main>
        @yield('content')
    </main>
    <footer class="bg-white text-black-50 text-capitalize py-3 shadow">
        {{ config('app.name')  }} | Ricardo Urvina Cordova {{date("Y")}}
    </footer>
</div>

</body>
</html>
