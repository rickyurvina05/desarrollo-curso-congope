<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProjectControllerR;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

App::setLocale('es');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//DB::listen(function ($auery){
//    var_dump($auery->sql);
//});
Route::get('/',function (){
    return view('home');
})->name('home');


Route::view('/about','about')->name('about');
Route::resource('projects',ProjectControllerR::class);
Route::resource('contacts',ContactController::class);
Route::resource('categories',CategoryController::class);
Route::resource('departments',DepartmentController::class);
Route::resource('notifications',NotificationController::class);
Route::resource('tasks',TaskController::class);
