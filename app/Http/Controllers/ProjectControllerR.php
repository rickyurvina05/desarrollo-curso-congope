<?php

namespace App\Http\Controllers;

use App\Events\ProjectCreated;
use App\Http\Requests\EditProjectRequest;
use App\Jobs\CreateProject;
use App\Models\Category;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectControllerR extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('category')->get();
//        $projects->pluck('cost')->sum();
//        $projects->sum('cost');
//        $projects->find(1);
//        $projects->modelKeys();
//        $projects->load('category');
//        $categories = Category::pluck('name', 'id');
//        $projects->loadMissing('category');//carga la relacion si aun noi la ha cargado
//        $projects->map(function ($project) {
//            return $project->title;
//        });
//        $projects->map->title;//mensajes de orden superior
//        $projects->each(function ($project){ return $project->title="Changed";});//cambia el nombre del titulo en todos los proyectos

        $categories = Category::pluck('name', 'id');

        return view('projects.index', compact('projects', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('projects.create')
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title' => ['required', 'unique:projects'],
                'description' => 'required|min:10',
                'category_id' => 'required|integer'
            ]
        );
        CreateProject::dispatch($request->all());
        return redirect()->route('projects.index')->with('status', 'Guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::with('category')->findOrFail($id);
        return view('projects.show', ['project' => $project]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);
        $categories = Category::pluck('name', 'id');
        return view('projects.edit', compact('project'))->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditProjectRequest $request, $id)
    {
        $project = Project::find($id);
        $project->update($request->validated());
        ProjectCreated::dispatch($project, $eventName = 'Proyecto Actualizado');
        return redirect()->route('projects.show', $project)->with('status', 'Actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();
        return redirect()->route('projects.index')->with('status', 'Eliminado exitosamente');
    }
}
