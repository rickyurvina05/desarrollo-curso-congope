<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {

//        Department::factory()
//            ->has(Contact::factory()->count(3))
//            ->create();

//        Department::factory()->hasContacts(3)->create();
//        Department::factory()->hasContacts(3,[
//            'email'=>null
//        ])->create();




        $departments = Department::get();
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        //3
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'name' => ['required', 'unique:departments'],
                'description' => ['required'],
            ]
        );

        Department::create($request->all());
        return redirect()->route('departments.index')->with('status', 'Guardado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
        return view('departments.show', [
            'department' => $department,
            'contacts' => $department->contacts->load('department')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
        return view('departments.edit', compact('department'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Department $department)
    {
        //
        $request->validate(
            [
                'name' => 'required',
                'description' => 'required'
            ]
        );
        $department->update($request->all());
        return redirect()->route('departments.show', $department)->with('status', 'Actualizado exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Department $department)
    {
        //
        $department->delete();
        return redirect()->route('departments.index')->with('status', 'Eliminado exitosamente');

    }
}
