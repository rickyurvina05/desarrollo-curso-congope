<?php

namespace App\Listeners;

use App\Events\ProjectCreated;
use App\Models\Notification;
use App\Models\Project;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SaveNotificationProjectCreated
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ProjectCreated $event
     * @return void
     */
    public function handle(ProjectCreated $event)
    {
        Notification::create([
            'name' => $event->eventName,
            'details' => $event->project,
        ]);
    }
}
